require("chromedriver");
const { Builder, By, Key, until } = require("selenium-webdriver");
var webdriver = require("selenium-webdriver");
var assert = require("chai").assert;

/* ------------------- Prueba boton 1  ---------------- */
describe("boton-1 201503515", function () {
  this.timeout(30000);
  let driver;
  let vars;
  //Verifica compatibilidad con el navegador
  beforeEach(async function () {
    driver = await new Builder().forBrowser("chrome").build();
    vars = {};
  });
  afterEach(async function () {
    await driver.quit();
  });
  it("boton1", async function () {
    //Ruta
    await driver.get("http://localhost:3000/");
    //DImensiones
    await driver.manage().window().setRect({ width: 994, height: 1063 });
    //
    await driver.findElement(By.css(".boton1:nth-child(4)")).click();
    await driver.close();
  });
});
/* ------------------- Prueba boton 2  ---------------- */
describe("boton2", function () {
  this.timeout(30000);
  let driver;
  let vars;
  beforeEach(async function () {
    driver = await new Builder().forBrowser("chrome").build();
    vars = {};
  });
  afterEach(async function () {
    await driver.quit();
  });
  it("boton2", async function () {
    await driver.get("http://localhost:3000/");
    await driver.manage().window().setRect({ width: 994, height: 1063 });
    await driver.findElement(By.css(".boton1:nth-child(5)")).click();
    await driver.close();
  });
});
